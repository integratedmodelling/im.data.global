namespace data.global.landcover;

@reliability(40)
model local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_1992,
	  local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_1993,
	  local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_1994,
	  local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_1995,
	  local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_1996,
      local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_1997,
      local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_1998,
      local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_1999,
      local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2000,
      local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2001,
      local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2002,
      local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2003,
      local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2004,
      local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2005,
      local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2006,
      local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2007,
      local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2008,
      local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2009,
      local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2010,
	  local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2011,
	  local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2012,
	  local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2013,
	  local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2014,
	  local:srwohl:im.data.global:im-data-global-landcover.cci_lc_300m_2015,
	  local:srwohl:im.data.global:im-data-global-landcover.c3s_lc_300m_2016,
	  local:srwohl:im.data.global:im-data-global-landcover.c3s_lc_300m_2017,
	  local:srwohl:im.data.global:im-data-global-landcover.c3s_lc_300m_2018,
	  local:rubencc:im.data.global:im-data-global-landcover.cci_lc_300m_2019_v2_1_1,
	  local:rubencc:im.data.global:im-data-global-landcover.cci_lc_300m_2020_v2_1_1,
	  local:rubencc:im.data.global:im-data-global-landcover.cci_lc_300m_2021,
	  local:rubencc:im.data.global:im-data-global-landcover.cci_lc_300m_2022
	as landcover:LandCoverType classified according to im:cci-encoding;  

model 	'local:rubencc:im.data.global:im-data-global-landcover.clc2000acc_v2018_20_4326',
		'local:rubencc:im.data.global:im-data-global-landcover.clc2006acc_v2018_20_4326',
		'local:rubencc:im.data.global:im-data-global-landcover.clc2012acc_v2018_20_4326',
		'local:rubencc:im.data.global:im-data-global-landcover.clc2018acc_v2018_20_4326'
	as landcover:LandCoverType named corine_accounts_ready_land_cover classified according to im:corine-encoding
 	over space(urn = 'local:srwohl:im.data.global:im-data-global-landcover_corine_extent'); 


private model each landcover:Urban earth:Region named urban_area
    observing landcover:LandCoverType named landcover_type
    using gis.features.extract(select = [landcover_type is landcover:ArtificialSurface]); 

private model local:alessio.bulckaen:im.data.global:impact.observatory.annual.landuse
	as landcover:LandCoverType named io_landuse classified into
		landcover:WaterBody if 1,
		landcover:Forest if 2,
		landcover:Wetland if 4,
		landcover:ArableLand if 5,
		landcover:ArtificialSurface if 7,
		landcover:BareArea if 8,
		landcover:GlacierAndPerpetualSnow if 9,
		landcover:SparseVegetation if 11;

//private model local:alessio.bulckaen:im.data.global:tests.a_1985
//	as landcover:LandCoverType named glc_fcs30d classified into

private model local:alessio.bulckaen:im.data.global:tests.a_1985,
	  local:alessio.bulckaen:im.data.global:tests.a_1990,
	  local:alessio.bulckaen:im.data.global:tests.a_1995,
	  local:alessio.bulckaen:im.data.global:tests.a_2000,
	  local:alessio.bulckaen:im.data.global:tests.a_2001,
	  local:alessio.bulckaen:im.data.global:tests.a_2002,
	  local:alessio.bulckaen:im.data.global:tests.a_2003,
	  local:alessio.bulckaen:im.data.global:tests.a_2004,
	  local:alessio.bulckaen:im.data.global:tests.a_2005,
	  local:alessio.bulckaen:im.data.global:tests.a_2006,
	  local:alessio.bulckaen:im.data.global:tests.a_2007,
//	  local:alessio.bulckaen:im.data.global:tests.a_2008,
	  local:alessio.bulckaen:im.data.global:tests.a_2009,
	  local:alessio.bulckaen:im.data.global:tests.a_2010,
	  local:alessio.bulckaen:im.data.global:tests.a_2011,
	  local:alessio.bulckaen:im.data.global:tests.a_2012,
	  local:alessio.bulckaen:im.data.global:tests.a_2013,
	  local:alessio.bulckaen:im.data.global:tests.a_2014,
	  local:alessio.bulckaen:im.data.global:tests.a_2015,
	  local:alessio.bulckaen:im.data.global:tests.a_2016,
	  local:alessio.bulckaen:im.data.global:tests.a_2017,
	  local:alessio.bulckaen:im.data.global:tests.a_2018,
	  local:alessio.bulckaen:im.data.global:tests.a_2019,
	  local:alessio.bulckaen:im.data.global:tests.a_2020,
	  local:alessio.bulckaen:im.data.global:tests.a_2021,
	  local:alessio.bulckaen:im.data.global:tests.a_2022	  
	as landcover:LandCoverType named essd_lc classified according to im:essd-econding;	